FROM ubuntu:18.04

ARG VIDEO_GID
ARG INPUT_GID

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    dbus \
    fuse \
    gconf-service \
    gconf2 \
    i965-va-driver \
    libappindicator1 \
    libcairo2 \
    libcanberra-gtk-module \
    libcanberra-gtk3-module \
    libcurl3-gnutls \
    libfreetype6 \
    libgdk-pixbuf2.0-0 \
    libgl1 \
    libgl1-mesa-dri \
    libgl1-mesa-glx \
    libgles2-mesa \
    libglib2.0-0 \
    libgtk2.0-0 \
    libjson-c3 \
    libnotify4 \
    libnss3 \
    libopus0 \
    libpango-1.0-0 \
    libpangocairo-1.0-0 \
    libsecret-1-0 \
    libsm6 \
    libsndio6.1 \
    libssl1.1 \
    libubsan0 \
    libuv1 \
    libva-drm2 \
    libva-glx2 \
    libva-x11-2 \
    libva2 \
    libxtst6 \
    libxxf86vm1 \
    mesa-va-drivers \
    mesa-vdpau-drivers \
    pulseaudio-utils \
    seahorse \
    unzip \
    vainfo \
    vdpau-va-driver \
    wget \
    xserver-xorg-video-intel \
    && rm -rf /var/lib/apt/lists/*

RUN useradd -ms /bin/bash shadow-user
RUN groupadd fuse
RUN groupmod -og ${VIDEO_GID:-44} video
RUN groupmod -og ${INPUT_GID:-97} input
RUN usermod -aG video shadow-user
RUN usermod -aG fuse shadow-user
RUN usermod -aG input shadow-user
RUN mkdir -p /home/shadow-user/AppImage/
RUN chmod 770 -R /home/shadow-user
ADD start_script.sh /home/shadow-user/start.sh
RUN chmod +x /home/shadow-user/start.sh

RUN wget --no-check-certificate https://update.shadow.tech/launcher/testing/linux/ubuntu_18.04/ShadowAlpha.zip -O /tmp/shadow-alpha.zip && \
    cd /tmp && \
    unzip shadow-alpha.zip && \
    cp ShadowAlpha.AppImage /home/shadow-user/AppImage/ShadowAlpha.AppImage

ADD https://gitlab.com/NicolasGuilloux/shadow-live-os/raw/arch-master/airootfs/etc/drirc /etc/drirc

WORKDIR /home/shadow-user/AppImage
RUN mkdir -p /home/shadow-user/.config/
RUN mkdir -p /home/shadow-user/.cache/blade/
RUN mkdir -p /home/shadow-user/.local/share/keyrings/
RUN chown shadow-user:shadow-user -R /home/shadow-user
USER shadow-user

ENV LD_LIBRARY_PATH=''
CMD /home/shadow-user/start.sh /home/shadow-user/AppImage/ShadowAlpha.AppImage
